﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carta_detras : MonoBehaviour
{
    public GameObject mazo;
    public GameObject It;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mazo = GameObject.Find("DeckPanel");
        It.transform.SetParent(mazo.transform);
        It.transform.localScale = new Vector3(16, 11, 11);
        It.transform.position = new Vector3(transform.position.x, transform.position.y);
    }
}

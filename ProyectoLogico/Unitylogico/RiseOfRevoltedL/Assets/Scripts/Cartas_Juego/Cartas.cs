﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName= "New Card", menuName="Cartas")]
public class Cartas : ScriptableObject
{
    public string nombre;
    public string descripcion;
    public Texture imagen;
    public Texture tipo;
    public int coste;
    public int daño;
    public int defensa;
    public int curacion;
    public int hpAumen;

    public bool PierdTurnoEnemy;
    public bool PierdTurnoTu;

    public bool Pifia;

    public bool Loki;

    public bool Manzana;

    public bool freecard;

    public bool Esquive;

    public bool Revivir;
    public int hpmenos;


    public int SacaCartas;
    public int RecuperaMana;
}

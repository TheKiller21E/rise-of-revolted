﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log(eventData.pointerDrag.name + " se lanzo hacia " + gameObject.name);
        Arrastrar a = eventData.pointerDrag.GetComponent<Arrastrar>();
        if (a != null)
        {
            a.parentToRetunTo = this.transform;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag==null)
        {
            return;
        }
        Arrastrar a = eventData.pointerDrag.GetComponent<Arrastrar>();
        if (a != null)
        {
            a.placeholderParent = this.transform;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
        {
            return;
        }
        Arrastrar a = eventData.pointerDrag.GetComponent<Arrastrar>();
        if (a != null && a.placeholderParent==this.transform)
        {
            a.placeholderParent = a.parentToRetunTo;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Mazo : MonoBehaviour
{
    public List<Cartas> mazo = new List<Cartas>();
    public List<Cartas> contenedor = new List<Cartas>();
    public static List<Cartas> staticDeck = new List<Cartas>();

    public int x;
    public GameObject datos;
    public static int deckSize;
    public GameObject cartasenmazo1;
    public GameObject cartasenmazo2;
    public GameObject cartasenmazo3;
    public GameObject cartasenmazo4;

    public GameObject ManoCartas;
    public GameObject CartaDet;

    public GameObject []Clones;

    public GameObject Mano;
    public int numCard;

    // Start is called before the first frame update
    void Start()
    {
       
        x = 0;
        deckSize = mazo.Count;

        for (int i = 0;i<deckSize; i++)   //poniendo  decksize todas se eligen aleatoriamente
        {
            x = Random.Range(0, 18);
            mazo[i] = datos.GetComponent<BaseCartas>().colecion[x];
        }

        StartCoroutine(StartGame());
    }

    void Update()
    {
        staticDeck = mazo;

        if (deckSize>=30)
        {
            cartasenmazo1.SetActive(true);
            cartasenmazo2.SetActive(true);
            cartasenmazo3.SetActive(true);
            cartasenmazo4.SetActive(true);
        }
        if (deckSize < 20)
        {
            cartasenmazo1.SetActive(false);
        }
        if (deckSize < 10)
        {
            cartasenmazo2.SetActive(false);
        }
        if (deckSize < 2)
        {
            cartasenmazo3.SetActive(false);
        }
        if (deckSize < 1)
        {
            cartasenmazo4.SetActive(false);
        }
        if (deckSize==0)
        {
            deckSize = mazo.Count;
        }

        numCard = Mano.transform.childCount;

        if (numCard>=7)
        {
            CardDisplay.drawX = 0;
        }
        else
        {
                StartCoroutine(Draw(CardDisplay.drawX));
                CardDisplay.drawX = 0;
        }

        if (Turnos.Turnoempieza == true)
        {
            if (numCard <= 0)
            {
                StartCoroutine(Draw(5));
            }
            if (numCard == 1)
            {
                StartCoroutine(Draw(4));
            }
            if (numCard == 2)
            {
                StartCoroutine(Draw(3));
            }
            if (numCard == 3)
            {
                StartCoroutine(Draw(2));
            }
            if (numCard == 4)
            {
                StartCoroutine(Draw(1));
            }

            Turnos.Turnoempieza = false;
        }
    }

    IEnumerator Example()
    {
        yield return new WaitForSeconds(1);
        Clones = GameObject.FindGameObjectsWithTag("Clone");

        foreach (GameObject Clone in Clones)
        {
            Destroy(Clone);
        }
    }

    IEnumerator StartGame()
    {

        for (int i = 0; i <= 4; i++)
        {
            yield return new WaitForSeconds(1);
            Instantiate(ManoCartas, transform.position, transform.rotation);
        }
    }

    public void barajear()
    {
        for (int i = 0; i < deckSize; i++)
        {
            contenedor[0] = mazo[i];
            int randomIndex = Random.Range(i, deckSize);
            mazo[i] = mazo[randomIndex];
            mazo[randomIndex] = contenedor[0];
        }

        Instantiate(CartaDet, transform.position, transform.rotation);
        StartCoroutine(Example());
    }

    IEnumerator Draw(int x)
    {
        for (int i = 0; i <x; i++)
        {
            yield return new WaitForSeconds(1f);
            Instantiate(ManoCartas, transform.position, transform.rotation);
        }
    }
}

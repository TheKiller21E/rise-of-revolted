﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class infocartas : MonoBehaviour
{
    public Cartas card;
    public Text nombretext;
    public Text descripcion;
    public RawImage imagen;
    public RawImage tipo;
    public Text costetext;
    public int daño;
    public int curacion;
    public int defensa;
    public int DrawXcards;
    public int RecupMana;
    public int nueva;

    void Start()
    {
        nueva = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (nueva<=1 && nueva>=0)
        {
            int x;
            x = Random.Range(0, 18);
            GameObject carta = GameObject.Find("CartasManager");
            card = carta.GetComponent<BaseCartas>().colecion[x];
            nueva--;
        }

        nombretext.text = card.nombre;
        descripcion.text = card.descripcion;
        imagen.texture = card.imagen;
        costetext.text = card.coste.ToString();
        tipo.texture = card.tipo;
        daño = card.daño;
        curacion = card.curacion;
        defensa = card.defensa;
        DrawXcards = card.SacaCartas;
        RecupMana = card.RecuperaMana;

    }

    public void lootnew()
    {
        nueva = 1;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CardDisplay : MonoBehaviour
{
    public Cartas card;
    public Text nombretext;
    public Text descripcion;
    public RawImage imagen;
    public RawImage tipo;

    public Text costetext;
    public int daño;
    public int curacion;
    public int defensa;

    public int hpaunment;
    public static int aunment;

    public bool LoseTurn;
    public static bool loseTurn;
    public bool LoseTurnPl;
    public static bool loseTurnPl;

    public int NumerodeCartasEnMazo;

    public bool SepuedeUsar;
    public bool Usada;
    public GameObject ZonadeBatalla;

    public static int drawX;
    public int DrawXcards;
    public int RecupMana;

    public bool pifia;

    public bool loki;

    public bool manzana;
    public static int manzanita;

    public bool FreeCard;

    public bool esquive;
    public static int esquivePl;

    public bool revive;
    public static bool Revive;
    public int hpcoste;


    /*[HideInInspector]
    public GameObject Enemigo;
    [HideInInspector]
    public GameObject Jugador;*/

    void Start()
    {
        SepuedeUsar = false;
        Usada = false;
        NumerodeCartasEnMazo = Mazo.deckSize;

        drawX = 0;
    }

    void Update()
    {
        /*Enemigo = GameObject.Find("enemigo");
        Jugador = GameObject.Find("Personaje");*/
        nombretext.text = card.nombre;
        descripcion.text = card.descripcion;
        imagen.texture = card.imagen;
        tipo.texture = card.tipo;
        costetext.text = card.coste.ToString();
        daño = card.daño;
        curacion = card.curacion;
        hpaunment = card.hpAumen;
        LoseTurn = card.PierdTurnoEnemy;
        LoseTurnPl = card.PierdTurnoTu;
        defensa = card.defensa;
        DrawXcards = card.SacaCartas;
        RecupMana = card.RecuperaMana;
        pifia = card.Pifia;
        loki = card.Loki;
        manzana = card.Manzana;
        FreeCard = card.freecard;
        esquive = card.Esquive;
        revive = card.Revivir;
        hpcoste = card.hpmenos;


        if (this.tag == "Clone")
        {
            card = Mazo.staticDeck[NumerodeCartasEnMazo - 1];
            NumerodeCartasEnMazo -= 1;
            Mazo.deckSize -= 1;
            this.tag = "Untagged";
        }
        
        if (Turnos.ManaActual>= card.coste && Usada==false && Turnos.isYourTurn==true)
        {
            SepuedeUsar = true;
        }
        else
        {
            SepuedeUsar = false;
        }

        if (SepuedeUsar==true)
        {
            gameObject.GetComponent<Arrastrar>().enabled = true;
        }
        else
        {
            gameObject.GetComponent<Arrastrar>().enabled = false;
        }

        ZonadeBatalla = GameObject.Find("Cartasjugadas");

        if (Usada==false && this.transform.parent == ZonadeBatalla.transform)
        {
            Usar();
        }

    }

    public void Usar()
    {
        loseTurn = LoseTurn;
        loseTurnPl = LoseTurnPl;

        if (pifia == true)
        {
            int xPifia = Random.Range(0, 11);
            pifiaProb(xPifia);
        }
        else
        {
            Enemy.defensa -= daño;
        }

        //Turnos.ManaActual -= card.coste;
        ManaGasto();
        Usada = true;
        Manaplus(RecupMana);
        drawX = DrawXcards;
        Curar();
        Player.defensa += defensa;
        Manzanas();
        EsquivePL();
        Revvivi();

        if (hpaunment>0)
        {
            Player.hpMax += hpaunment;
            Player.hpactual += hpaunment;
            aunment = 3;
        }

        Destroy(gameObject);
    }

    public void Manaplus(int x)
    {
        Turnos.ManaActual += x;
    }

    public void Curar()
    {
        if (loki==true)
        {
            int porcentaje = (Player.hpMax * 20) / 100;
            if (Player.hpactual + porcentaje > Player.hpMax)
            {
                Player.hpactual = Player.hpMax;
            }
            else
            {
                Player.hpactual += porcentaje;
            }
            loki = false;
        }
        else
        {
            if (Player.hpactual+card.curacion > Player.hpMax)
            {
                Player.hpactual = Player.hpMax;
            }
            else
            {
                Player.hpactual += curacion;
            }
        }
    }

    public void pifiaProb(int x)
    {
        switch (x)
        {
            default: pifia = false; break;
            case 1: pifia = false; break;
            case 2: pifia = false; break;
            case 3: pifia = false; break;
            case 4: Enemy.defensa -= 40; pifia = false; break;
            case 5: pifia = false; break;
            case 6: pifia = false; break;
            case 7: pifia = false; break;
            case 8: Enemy.defensa -= 40 * 3; pifia = false; break;
            case 9: pifia = false; break;
            case 10: Enemy.defensa -= 40 * 2; pifia = false; break;
        }
    }

    public void Manzanas()
    {
        if (manzana==true)
        {
            manzanita = 1;
        }

        if (manzanita == 1)
        {
            manzana = true;
        }
        else
        {
            manzana = false;
        }
    }   
    
    public void Revvivi()
    {
        if (revive==true)
        {
            Revive = true;
        }

        if (Revive == true)
        {
            revive = true;
        }
        else
        {
            revive = false;
        }
    }   
    
    public void EsquivePL()
    {
        if (esquive==true)
        {
            esquivePl = 1;
        }

        if (esquivePl == 1)
        {
            esquive = true;

        }
        else
        {
            esquive = false;
        }
    }   

    public void ManaGasto()
    {
        if (FreeCard == true)
        {
            Player.noFreeCard = 1;
        }
        if (Player.noFreeCard == 1 && FreeCard == false)
        {
            Turnos.ManaActual -= card.coste;
            Turnos.ManaActual += card.coste;

            Player.noFreeCard = 0;
            Turnos.ManaActual -= 6;
        }
        else
        {
            Turnos.ManaActual -= card.coste;
            Player.hpMax -= hpcoste;
        }
    }
}

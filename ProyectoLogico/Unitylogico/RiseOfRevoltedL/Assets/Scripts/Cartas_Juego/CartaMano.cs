﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartaMano : MonoBehaviour
{
    public GameObject Mano;
    public GameObject It;

    void Start()
    {
        Mano = GameObject.Find("Mano");
        It.transform.SetParent(Mano.transform);
        It.transform.position = new Vector3(transform.position.x, transform.position.y);
        It.transform.localScale = new Vector3(2, 11, 11);
        It.transform.eulerAngles = new Vector3(0, 0, 0);

    }

    void Update()
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class Tiempo : MonoBehaviour
{
    public int x;
    public VideoPlayer video;
    void Awake()
    {

        video.Play();
        video.loopPointReached += CheckOver;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(x);
        }
    }
    void CheckOver(UnityEngine.Video.VideoPlayer vp)
    {
        SceneManager.LoadScene(x);//the scene that you want to load after the video has ended.
    }
}

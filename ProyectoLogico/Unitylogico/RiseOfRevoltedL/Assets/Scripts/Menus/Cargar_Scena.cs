﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Cargar_Scena : MonoBehaviour
{
    void Update()
    {
        if (Niveles.nvl>7)
        {
            StartCoroutine(pasar());
        }
    }

    public void Menu()
    {
        SceneManager.LoadScene("Escena_Menus");
    }

    public void Juego()
    {
        SceneManager.LoadScene("Escena_Juego");
    }

    public void Wiki()
    {
        SceneManager.LoadScene("EscenaWiki");
    }

    public void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    public void Salir()
    {
        Application.Quit();
    }

    IEnumerator pasar()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(6);
        Niveles.nvl = 1;
    }

}

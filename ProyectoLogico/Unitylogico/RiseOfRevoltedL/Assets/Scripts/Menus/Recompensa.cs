﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recompensa : MonoBehaviour
{

    public GameObject MenuUI;
    public bool active;
    public static bool activo;

    // Update is called once per frame
    void Update()
    {
        active = activo;
        if (Enemy.hpactual<=0 && activo==true)
        {
            MenuUI.SetActive(true);
            Time.timeScale = 0;
            activo = false;
        }
        active = activo;
    }

    public void CartaElegida()
    {
        Time.timeScale = 1;
        MenuUI.SetActive(false);
        GameObject turno = GameObject.Find("SistemadeTurnos");
        turno.GetComponent<Turnos>().Reset();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Opciones : MonoBehaviour
{
    public Dropdown resolucionSelector;

    Resolution[] resolutions;

    void Start()
    {
        resolutions = Screen.resolutions;

        resolucionSelector.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolucionSelector.AddOptions(options);
        resolucionSelector.value = currentResolutionIndex;
        resolucionSelector.RefreshShownValue();
    }

    public void SetResolution(int resolutionindex)
    {
        Resolution resolucion = resolutions[resolutionindex];
        Screen.SetResolution(resolucion.width, resolucion.height, Screen.fullScreen);
    }

    public void PantallaCompleta(bool PantallaCompleta)
    {
        Screen.fullScreen = PantallaCompleta;
    }

    [Range(0.0f, 1.0f)]
    public GameObject Volumen;
    float masterVolume;
    void Update()
    {
        masterVolume = GameObject.Find("Audio").GetComponent<Slider>().value;
        AudioListener.volume = masterVolume;
    }
}

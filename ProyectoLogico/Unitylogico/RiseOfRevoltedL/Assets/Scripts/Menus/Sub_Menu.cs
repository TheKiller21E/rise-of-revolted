﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sub_Menu : MonoBehaviour
{
    public GameObject JugarMenu;
    public GameObject OpcionesMenu;
    public GameObject Botones;
    public bool SubMenu;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SubMenu)
            {
                DesactivarJugarMenu();
                DesactivarOpciones();
                ActivarBotones();
                SubMenu = false;
            }
        }
    }

    public void DesactivarBotones()
    {
        SubMenu = false;
        Botones.SetActive(false);
    }

    public void ActivarBotones()
    {
        SubMenu = true;
        Botones.SetActive(true);
    }

    public void ActivarJugarMenu()
    {
        SubMenu = true;
        JugarMenu.SetActive(true);
    }

    public void DesactivarJugarMenu()
    {
        SubMenu = false;
        JugarMenu.SetActive(false);
        ActivarBotones();
    }
    public void ActivarOpciones()
    {
        SubMenu = true;
        OpcionesMenu.SetActive(true);
    }

    public void DesactivarOpciones()
    {
        SubMenu = false;
        OpcionesMenu.SetActive(false);
        ActivarBotones();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class NivelEnemy : MonoBehaviour
{
    public Texture arpia;
    public Texture ciclope;
    public Texture medusa;
    public Texture mix;
    public Texture hydra;
    public Texture hogera;
    public Texture zeus;
    public Text descrip;
    public GameObject Cuadro;
    public RawImage enfrent;



    public void AparecerArpi()
    {
        Cuadro.SetActive(true);
        enfrent.texture = arpia;
        descrip.text = "Arpia";
    }

    public void AparecerCiclop()
    {
        Cuadro.SetActive(true);
        enfrent.texture = ciclope;
        descrip.text = "Ciclope";
    }

    public void AparecerTMedus()
    {
        Cuadro.SetActive(true);
        enfrent.texture = medusa;
        descrip.text = "Medusa";
    }

    public void AparecerMix()
    {
        Cuadro.SetActive(true);
        enfrent.texture = mix;
        descrip.text = "Ciclope y Arpia";
    }

    public void Aparecerhydra()
    {
        Cuadro.SetActive(true);
        enfrent.texture = hydra;
        descrip.text = "Hydra";
    }

    public void Aparecerhogere()
    {
        Cuadro.SetActive(true);
        enfrent.texture = hogera;
        descrip.text = "Hoguera";
    }

    public void AparecerZeus()
    {
        Cuadro.SetActive(true);
        enfrent.texture = zeus;
        descrip.text = "Zeus";
    }

    public void off()
    {
        Cuadro.SetActive(false);
    }
}

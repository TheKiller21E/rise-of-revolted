﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Pausa : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public GameObject Opciones;
    public GameObject Map;
    public bool mapopen;
    public bool isPaused;

    // Update is called once per frame
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
        }
        if (isPaused && !mapopen)
        {
            ActivateMenu();
        }
        else
        {
            DesactivateMenu();
            DesactivarOpciones();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (mapopen)
            {
                DesactivateMapa();
            }
        }

    }

   public void ActivateMenu()
   {
        Time.timeScale = 0;
        pauseMenuUI.SetActive(true);
        isPaused = true;
   }

    public void DesactivateMenu()
    {
        Time.timeScale = 1;
        pauseMenuUI.SetActive(false);
        isPaused = false;
    }

    public void ActivarOpciones()
    {
        Opciones.SetActive(true);
    }

    public void DesactivarOpciones()
    {
        Opciones.SetActive(false);
    }

    public void Activatemapa()
    {
        Time.timeScale = 0;
        Map.SetActive(true);
        mapopen = true;
    }

    public void DesactivateMapa()
    {    
        Time.timeScale = 1;
        Map.SetActive(false);
        mapopen = false;
    }
}

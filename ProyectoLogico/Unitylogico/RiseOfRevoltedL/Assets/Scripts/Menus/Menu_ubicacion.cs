﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Menu_ubicacion : MonoBehaviour
{
    public GameObject circ1;
    public GameObject circ2;
    public GameObject circ3;
    public GameObject circ4;
    public GameObject circ5;
    public GameObject circ6;
    public GameObject circ7;
    public GameObject cross1;
    public GameObject cross2;
    public GameObject cross3;
    public GameObject cross4;
    public GameObject cross5;
    public GameObject cross6;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        switch (Niveles.nvl)
        {
            case 1: circ1.SetActive(true); break;
            case 2: circ2.SetActive(true); cross1.SetActive(true); circ1.SetActive(false); break;
            case 3: circ3.SetActive(true); cross2.SetActive(true); circ2.SetActive(false); break;
            case 4: circ4.SetActive(true); cross3.SetActive(true); circ3.SetActive(false); break;
            case 5: circ5.SetActive(true); cross4.SetActive(true); circ4.SetActive(false); break;
            case 6: circ6.SetActive(true); cross5.SetActive(true); circ5.SetActive(false); break;
            case 7: circ7.SetActive(true); cross6.SetActive(true); circ6.SetActive(false); break;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Background : MonoBehaviour
{
    public Texture bkgroundDia;
    public Texture bkgroundNoche;

    // Update is called once per frame
    void Update()
    {
        GameObject fondo = this.gameObject;
        if (Niveles.nvl<=6)
        {
            fondo.GetComponent<RawImage>().texture=bkgroundDia;
        }
        else
        {
            fondo.GetComponent<RawImage>().texture = bkgroundNoche;

        }
    }
}

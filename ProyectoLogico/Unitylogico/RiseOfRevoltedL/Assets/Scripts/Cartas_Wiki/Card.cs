﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Card 
{

    public string cardName;
    [TextArea(1, 3)]
    public string cardDes;
    public cardClass cardClass;
    public Sprite cClass;
    public Sprite cardSprite;

    public int cardCost;
}

public enum cardClass
{
    Griega,
    Nórdica,
    Egipcia,
    Base,
}

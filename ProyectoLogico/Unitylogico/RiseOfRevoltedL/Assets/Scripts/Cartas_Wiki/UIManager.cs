﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    public GameObject botonclased;
    public GameObject botonclasea;
    public GameObject botona;
    public GameObject botond;

    public CardManager cardManager;
    public GameObject[] cardSlots;
    //2
    public int page;
    public Text pageText;

    int _class;

    //[SerializeField] private bool isSearch;
    //[SerializeField] private int totalNumbers;

    public void Next()
    {
        if (page >= Mathf.Floor((cardManager.cartas.Count - 1) / 10))
        {
            page = 0;
        }
        else
        {
            page++;
        }
        Debug.Log(page);
        DisplayCards();
    }

    public void Back()
    {
        if (page <= 0)
        {
            page = Mathf.FloorToInt((cardManager.cartas.Count - 1) / 10);
        }
        else
        {
            page--;
        }
        Debug.Log(page);
        DisplayCards();
    }
    public void NextClass()
    {
        if (page >= Mathf.Floor((cardManager.grecia.Count - 1) / 7))
        {
            page = 0;
        }
        else
        {
            page++;
        }
        Debug.Log("avanzo, papu");
        SearchByClass(cardClass.Griega.ToString("g"));
    }

    public void BackClass()
    {
        if (page <= 0)
        {
            page = Mathf.FloorToInt((cardManager.grecia.Count - 1) / 7);
        }
        else
        {
            page--;
        }
        Debug.Log("retrocedo, papu");
        SearchByClass(cardClass.Griega.ToString("g"));
    }
    public void Start()
    {
        DisplayCards();

    }

    public void Update()
    {
        UpdatePage();

    }

    public void UpdatePage()
    {  
            // pageText.text = (page + 1).ToString();

            pageText.text = (page + 1) + "/" + (Mathf.Ceil(cardSlots.Length / 7)).ToString();
    }

    public void SearchByCost(int _cost)
    {
        //isSearch = true;
        //totalNumbers = 0;
        for (int i = 0; i < cardManager.cartas.Count; i++)
        {
            if (_cost < 7)
            {
                if (cardManager.cartas[i].cardCost == _cost)
                {
                    cardSlots[i].gameObject.SetActive(true);

                    cardSlots[i].GetComponent<Image>().sprite = cardManager.cartas[i].cClass;
                    cardSlots[i].transform.GetChild(1).GetComponent<Text>().text = cardManager.cartas[i].cardDes;
                    cardSlots[i].transform.GetChild(2).GetComponent<Text>().text = cardManager.cartas[i].cardCost.ToString();
                    cardSlots[i].transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = cardManager.cartas[i].cardSprite;
                    cardSlots[i].transform.GetChild(3).GetComponent<Text>().text = cardManager.cartas[i].cardName;
                    
                }
                else
                {
                    cardSlots[i].gameObject.SetActive(false);
                }
            }
            else
            {
                if (cardManager.cartas[i].cardCost >= _cost)
                {
                    cardSlots[i].gameObject.SetActive(true);

                    cardSlots[i].GetComponent<Image>().sprite = cardManager.cartas[i].cClass;
                    cardSlots[i].transform.GetChild(1).GetComponent<Text>().text = cardManager.cartas[i].cardDes;
                    cardSlots[i].transform.GetChild(2).GetComponent<Text>().text = cardManager.cartas[i].cardCost.ToString();
                    cardSlots[i].transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = cardManager.cartas[i].cardSprite;
                    cardSlots[i].transform.GetChild(3).GetComponent<Text>().text = cardManager.cartas[i].cardName;
                    
                }
                else
                {
                    cardSlots[i].gameObject.SetActive(false);
                }
            }
            
        }
    }
    
    public void SearchByClass(string _class)
    {
        GameObject buttonMenuClassD = GameObject.FindWithTag("ButtonMenuClassD");
        GameObject buttonMenuClassA = GameObject.FindWithTag("ButtonMenuClassA");

        if (botonclased.activeSelf == false)
        {
            botonclased.SetActive(true);
            botond.SetActive(false);
            
        }
        if (botonclasea.activeSelf == false)
        {
            botonclasea.SetActive(true);
            botona.SetActive(false);
        }

        Debug.Log("He activado los botones de clase");
        for (int i = 0; i < cardManager.grecia.Count; i++)
        {
            if (cardManager.grecia[i].cardClass.ToString() == _class)
            {
                if (i >= page * 10 && i < (page + 1) * 10)
                {
                    cardSlots[i].gameObject.SetActive(true);
                    cardSlots[i].GetComponent<Image>().sprite = cardManager.grecia[i].cClass;
                    cardSlots[i].transform.GetChild(1).GetComponent<Text>().text = cardManager.grecia[i].cardDes;
                    cardSlots[i].transform.GetChild(2).GetComponent<Text>().text = cardManager.grecia[i].cardCost.ToString();
                    cardSlots[i].transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = cardManager.grecia[i].cardSprite;
                    cardSlots[i].transform.GetChild(3).GetComponent<Text>().text = cardManager.grecia[i].cardName;
                }
                else
                {
                    cardSlots[i].gameObject.SetActive(false);
                }               
            }
            
        }
    }

    public void DisplayCards()
    {
        GameObject buttonMenuClassD = GameObject.FindWithTag("ButtonMenuClassD");
        GameObject buttonMenuClassA = GameObject.FindWithTag("ButtonMenuClassA");
        
        if (buttonMenuClassD != null)
        {
            buttonMenuClassD.SetActive(false);
        }
        if (buttonMenuClassA != null)
        {
            buttonMenuClassA.SetActive(false);
        }
        if (botond.activeSelf == false)
        {
            botond.SetActive(true);
        }
        if (botona.activeSelf == false)
        {
            botona.SetActive(true);
        }
        Debug.Log("He falseado los botones de clase");

        for (int i = 0; i < cardManager.cartas.Count; i++)
        {
            if (i >= page * 10 && i < (page + 1) * 10)
            {
                cardSlots[i].gameObject.SetActive(true);

                cardSlots[i].GetComponent<Image>().sprite = cardManager.cartas[i].cClass;
                cardSlots[i].transform.GetChild(1).GetComponent<Text>().text = cardManager.cartas[i].cardDes;
                cardSlots[i].transform.GetChild(2).GetComponent<Text>().text = cardManager.cartas[i].cardCost.ToString();
                cardSlots[i].transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = cardManager.cartas[i].cardSprite;
                cardSlots[i].transform.GetChild(3).GetComponent<Text>().text = cardManager.cartas[i].cardName;
            }
            else
            {
                cardSlots[i].gameObject.SetActive(false);
            }
        }
    }
    
}


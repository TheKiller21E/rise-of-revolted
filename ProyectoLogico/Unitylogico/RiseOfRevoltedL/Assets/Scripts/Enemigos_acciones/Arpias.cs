﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arpias : MonoBehaviour
{

    public int x;
    public int porcentaje;
    public bool esquive;
    public static bool esquivenem;
    public GameObject esqui;
    public GameObject manz;



    void Update()
    {
        if (CardDisplay.manzanita == 1)
        {
            manz.SetActive(true);
        }
        else
        {
            manz.SetActive(false);
        }

        porcentaje = (Enemy.hpactual * 100) / Enemy.hpMax;

        if (Turnos.isEnemyTurn==true && Enemy.tocaT >= 1)
        {
            if (CardDisplay.manzanita==1)
            {
                StartCoroutine(confusion());
            }
            else
            {
                esquive = false;
                esquivenem = esquive;
                esqui.SetActive(false);
                if (porcentaje<=25)
                {
                    StartCoroutine(curar());
                }
                else
                {
                    x = Random.Range(0, 4);
                    StartCoroutine(Acciones(x));
                }
            }
            CardDisplay.manzanita = 0;
            Enemy.tocaT -= 1;
            StartCoroutine(terminarturno());
        }
    }

    IEnumerator Acciones(int numer)
    {
        yield return new WaitForSeconds(1.5f);
        switch (numer)
        {
            default: Player.defensa -= 15; break; //pluma 
            case 1:  Player.defensa -= 20; break; //arañazo 
            case 2:  Player.defensa -= 25; break; //pico
            case 3:
                if (Enemy.hpactual == Enemy.hpMax)
                {
                    Enemy.defensa += 20;
                }
                else if (Enemy.hpactual<Enemy.hpMax && Enemy.hpactual<=65)
                {
                    Enemy.hpactual += 15;
                }
                else
                {
                    Enemy.hpactual += 20;
                }
                break;
        }
    }

    IEnumerator curar()
    {
        yield return new WaitForSeconds(1.5f);
        Enemy.hpactual += 30;
        esquive = true;
        esqui.SetActive(true);
        esquivenem = esquive;
    }

    IEnumerator terminarturno()
    {
        yield return new WaitForSeconds(3f);
        GameObject go = GameObject.Find("SistemadeTurnos");
        Turnos ec = go.GetComponent<Turnos>();
        ec.TerminarTurnoEnemigo();
    }

    IEnumerator confusion()
    {
        yield return new WaitForSeconds(1.5f);
        Enemy.defensa -= 40;
    }
}

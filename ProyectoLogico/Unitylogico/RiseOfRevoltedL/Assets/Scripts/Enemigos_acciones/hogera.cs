﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class hogera : MonoBehaviour
{
    public int porcentaje;
    public int porcentajeCura;
    public GameObject boton;
    public GameObject botton;
    public GameObject tapa;
    public Text cantcura;


    void Update()
    {
        tapa.SetActive(true);
        botton.GetComponent<Button>().enabled = false;
        porcentaje = (Player.hpactual * 100) / Player.hpMax;
        porcentajeCura = (30 * Player.hpMax) / 100;
        cantcura.text = porcentajeCura.ToString();
    }

    public void nxtlvl()
    {
        Player.hpMax += 20;
        StartCoroutine(curar());
        boton.GetComponent<Button>().enabled = false;
    }

    IEnumerator curar()
    {
        yield return new WaitForSeconds(0.5f);
        if (Player.hpactual + porcentajeCura > Player.hpMax)
        {
            Player.hpactual = Player.hpMax;
        }
        else
        {
           Player.hpactual += porcentajeCura;
        }
        StartCoroutine(terminarturno());
    }

    IEnumerator terminarturno()
    {
        yield return new WaitForSeconds(1.5f);
        Niveles.nvl++;
        botton.GetComponent<Button>().enabled = true;
        tapa.SetActive(false);
        gameObject.SetActive(false);
    }
}

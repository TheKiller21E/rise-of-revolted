﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hydra : MonoBehaviour
{
    public int x;
    public int porcentaje = 0;
    public GameObject manz;
    public GameObject truedamage;


    void Update()
    {
        if (CardDisplay.manzanita == 1)
        {
            manz.SetActive(true);
        }
        else
        {
            manz.SetActive(false);
        }

        if (Turnos.isEnemyTurn == true && Enemy.tocaT >= 1)
        {
            if (CardDisplay.manzanita == 1)
            {
                StartCoroutine(confusion());
            }
            else
            {
                if (porcentaje == 3)
                {
                    StartCoroutine(morder());
                    porcentaje = 0;
                }
                else
                {
                    x = Random.Range(0, 4);
                    if (x==1)
                    {
                        truedamage.SetActive(true);
                    }
                    else
                    {
                        truedamage.SetActive(false);
                    }
                    StartCoroutine(Acciones(x));
                    porcentaje++;
                }
            }
            CardDisplay.manzanita = 0;
            Enemy.tocaT -= 1;
            StartCoroutine(terminarturno());
        }
    }

    IEnumerator Acciones(int numer)
    {
        yield return new WaitForSeconds(1.5f);
        switch (numer)
        {
            default: Enemy.defensa += 50; Enemy.hpactual += 30; break; //curaescudo 
            case 1: Player.defensa = 0; Player.defensa -= 40; break; //ataque dircto rompe escudo 
            case 2: Enemy.defensa += 90; break; //escudo
            case 3: Player.defensa -= 40; break; //atk normal
        }
    }

    IEnumerator morder()
    {
        yield return new WaitForSeconds(1.5f);
        Player.defensa -= 70;
    }

    IEnumerator terminarturno()
    {
        yield return new WaitForSeconds(3f);
        GameObject go = GameObject.Find("SistemadeTurnos");
        Turnos ec = go.GetComponent<Turnos>();
        ec.TerminarTurnoEnemigo();
    }

    IEnumerator confusion()
    {
        yield return new WaitForSeconds(1.5f);
        Enemy.defensa -= 40;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ciclope : MonoBehaviour
{
    public int x;
    public int porcentaje=0;
    public GameObject manz;


    void Update()
    {
        if (CardDisplay.manzanita == 1)
        {
            manz.SetActive(true);
        }
        else
        {
            manz.SetActive(false);
        }

        if (Turnos.isEnemyTurn == true && Enemy.tocaT >= 1)
        {
            if (CardDisplay.manzanita == 1)
            {
                StartCoroutine(confusion());
            }
            else
            {
                if (porcentaje == 2)
                {
                    StartCoroutine(mazazo());
                    porcentaje = 0;
                }
                else
                {
                    x = Random.Range(0, 3);
                    StartCoroutine(Acciones(x));
                    porcentaje++;
                }
            }
            CardDisplay.manzanita = 0;
            Enemy.tocaT -= 1;
            StartCoroutine(terminarturno());
        }
    }

    IEnumerator Acciones(int numer)
    {
        yield return new WaitForSeconds(1.5f);
        switch (numer)
        {
            default: Enemy.defensa += 60; break; //Escudote 
            case 1: Player.defensa -= 25; break; //puñetazo 
            case 2: Player.defensa -= 38; break; //pisoton
        }
    }

    IEnumerator mazazo()
    {
        yield return new WaitForSeconds(1.5f);
        Player.defensa -= 60;
    }

    IEnumerator terminarturno()
    {
        yield return new WaitForSeconds(3f);
        GameObject go = GameObject.Find("SistemadeTurnos");
        Turnos ec = go.GetComponent<Turnos>();
        ec.TerminarTurnoEnemigo();
    }

    IEnumerator confusion()
    {
        yield return new WaitForSeconds(1.5f);
        Enemy.defensa -= 40;
    }
}

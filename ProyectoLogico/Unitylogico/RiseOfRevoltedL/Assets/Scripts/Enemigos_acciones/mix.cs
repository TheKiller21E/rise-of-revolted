﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mix : MonoBehaviour
{
    public int x;
    public int y;
    public int porcentaje = 0;
    public int npi;
    public GameObject ciclope;
    public GameObject manz;


    void Update()
    {
        if (CardDisplay.manzanita == 1)
        {
            manz.SetActive(true);
        }
        else
        {
            manz.SetActive(false);
        }

        npi = (Enemy.hpactual * 100) / Enemy.hpMax;

        if (Turnos.isEnemyTurn == true && Enemy.tocaT >= 1)
        {
            if (CardDisplay.manzanita == 1)
            {
                StartCoroutine(confusion());
            }
            else
            {
                if (npi>=50)
                {
                    if (porcentaje == 2)
                    {
                        StartCoroutine(mazazo());
                        porcentaje = 0;
                    }
                    else
                    {
                        x = Random.Range(0, 3);
                        StartCoroutine(Acciones(x));
                        porcentaje++;
                    }

                    if (npi <= 25)
                    {
                        StartCoroutine(curar());
                    }
                    else
                    {
                        y = Random.Range(0, 4);
                        StartCoroutine(action(y));
                    }

                }
                else
                {
                    ciclope.SetActive(false);
                    if (npi <= 10)
                    {
                         StartCoroutine(curar());
                    }
                    else
                    {
                         y = Random.Range(0, 4);
                         StartCoroutine(action(y));
                    }
                }
            }

            CardDisplay.manzanita = 0;
            Enemy.tocaT -= 1;
            StartCoroutine(terminarturno());
        }
    }

    IEnumerator Acciones(int numer)
    {
        yield return new WaitForSeconds(1f);
        switch (numer)
        {
            default: Enemy.defensa += 60; break; //Escudote 
            case 1: Player.defensa -= 25; break; //puñetazo 
            case 2: Player.defensa -= 38; break; //pisoton
        }
    }

    IEnumerator action(int numer)
    {
        yield return new WaitForSeconds(1.8f);
        switch (numer)
        {
            default: Player.defensa -= 15; break; //pluma 
            case 1: Player.defensa -= 20; break; //arañazo 
            case 2: Player.defensa -= 25; break; //pico
            case 3:
                if (Enemy.hpactual == Enemy.hpMax)
                {
                    Enemy.defensa += 20;
                }
                else if (Enemy.hpactual < Enemy.hpMax && Enemy.hpactual <= 65)
                {
                    Enemy.hpactual += 15;
                }
                break;
        }
    }

        IEnumerator mazazo()
        {
            yield return new WaitForSeconds(1.5f);
            Player.defensa -= 60;
        }

        IEnumerator curar()
        {
            yield return new WaitForSeconds(1.5f);
            Enemy.hpactual += 25;
        }

        IEnumerator terminarturno()
        {
            yield return new WaitForSeconds(3f);
            GameObject go = GameObject.Find("SistemadeTurnos");
            Turnos ec = go.GetComponent<Turnos>();
            ec.TerminarTurnoEnemigo();
        }

    IEnumerator confusion()
    {
        yield return new WaitForSeconds(1.5f);
        Enemy.defensa -= 40;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medusa : MonoBehaviour
{
    public int x;
    public bool esquive;
    public static bool esquivenem;
    public int chupar=0;
    public GameObject esqui;
    public GameObject manz;

    void Update()
    {
        if (CardDisplay.manzanita == 1)
        {
            manz.SetActive(true);
        }
        else
        {
            manz.SetActive(false);
        }

        if (Turnos.isEnemyTurn == true && Enemy.tocaT >= 1)
        {
            if (CardDisplay.manzanita == 1)
            {
                StartCoroutine(confusion());
            }
            else
            {
                esquive = false;
                esquivenem = esquive;
                esqui.SetActive(false);
                if (chupar == 3)
                {
                    StartCoroutine(piedra());
                    chupar = 0;
                }
                else
                {
                    x = Random.Range(0, 3);
                    StartCoroutine(Acciones(x));
                    chupar++;
                }
            }
            CardDisplay.manzanita = 0;
            Enemy.tocaT -= 1;
            StartCoroutine(terminarturno());
        }
    }

    IEnumerator Acciones(int numer)
    {
        yield return new WaitForSeconds(1.5f);
        switch (numer)
        {
            default: Player.defensa -= 20; Enemy.hpactual += 10; break; //xxxx  
            case 1:
                Player.defensa -= 30; break; //vivora
            case 2:
                Player.defensa -= 15; esquive = true; esquivenem = true; esqui.SetActive(true); break;
        }
    }

    IEnumerator piedra()
    {
        yield return new WaitForSeconds(1.5f);
        Enemy.hpactual += 40;
        Player.defensa -= 40;
    }

    IEnumerator terminarturno()
    {
        yield return new WaitForSeconds(3f);
        GameObject go = GameObject.Find("SistemadeTurnos");
        Turnos ec = go.GetComponent<Turnos>();
        ec.TerminarTurnoEnemigo();
    }

    IEnumerator confusion()
    {
        yield return new WaitForSeconds(1.5f);
        Enemy.defensa -= 40;
    }
}

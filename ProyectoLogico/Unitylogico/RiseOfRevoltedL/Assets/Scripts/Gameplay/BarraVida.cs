﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraVida : MonoBehaviour
{

    public Slider slider;
    public Text hptext;

    public void SetMaxHealth(int health)
    {
        slider.value = health;
        slider.maxValue = health;
    }


    public void SetHealth(int healt)
    {
        slider.value = healt;
    } 

    void Update()
    {
        slider.maxValue = Player.hpMax;
        hptext.text = Player.hpactual + " / " + Player.hpMax;
    }
}

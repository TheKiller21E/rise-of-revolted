﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int currenthealth;
    public int maxHealth = 300;
    public static int hpactual;
    public static int hpMax;
    public VidaEnemigo vidaEnemigo;
    public int Defensa=0;
    public static int defensa;
    public bool recomp;
    public int tocaN;
    public static int tocaT;


    // Start is called before the first frame update
    void Start()
    {
        defensa = Defensa;
        currenthealth = maxHealth;
        vidaEnemigo.SetMaxHealth(maxHealth);
        hpactual=currenthealth;
        hpMax = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (Turnos.isYourTurn == true)
        {
            tocaT = 1;
            tocaN = tocaT;
        }

        if (defensa <= 0)
        {
            Defensa = defensa;
            hpactual += defensa;

            if (Medusa.esquivenem == true || Arpias.esquivenem == true)
            {
                hpactual -= defensa;
            }

            defensa = 0;
        }
        else
        {
           Defensa = defensa;
        }


        maxHealth = hpMax;
        currenthealth = hpactual;
        hpMax = maxHealth;
        vidaEnemigo.SetHealth(currenthealth);

        if (hpactual<=0)
        {
            if (recomp==true)
            {
                Recompensa.activo = true;
            }
            else
            {
                Recompensa.activo = false;
            }
            Player.hpMax += 20;
            Player.hpactual += 20;
            Niveles.nvl++;
            CardDisplay.aunment = 0;
            Turnos.EnemyTurnPerder = 0;
            Turnos.PlayerTurnPerder = 0;
            CardDisplay.manzanita = 0;
            this.gameObject.SetActive(false);
        }
    }
}

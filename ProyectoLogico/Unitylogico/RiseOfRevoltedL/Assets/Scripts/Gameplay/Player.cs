﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int currenthealth;
    public int maxHealth = 100;
    public static int hpactual;
    public static int hpMax;
    public BarraVida barraVida;
    public int Defensa=0;
    public static int defensa;
    public bool HpSuma;
    public int TurnosHpSuma;
    public static int noFreeCard;
    public int FreeCard;
    public GameObject brebaj;
    public GameObject esquiv;
    public GameObject Rev;
    public bool Revivio;



    // Start is called before the first frame update
    void Start()
    {
        defensa = Defensa;
        currenthealth = maxHealth;
        barraVida.SetMaxHealth(maxHealth);
        hpactual = currenthealth;
        hpMax = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        brebaje();
        esquive();
        CristoRey();
        FreeCard = noFreeCard;
        TurnosHpSuma = CardDisplay.aunment;
        if (defensa <= 0)
        {
            Defensa = defensa;
            hpactual += defensa;

            if (CardDisplay.esquivePl==1)
            {
                if (defensa<=0)
                {
                    hpactual -= defensa;
                }
            }

            defensa = 0;
        }
        else
        {
            Defensa = defensa;
        }

        if (CardDisplay.aunment>0)
        {
            HpSuma = true;
        }
        else if (CardDisplay.aunment==0 && HpSuma==true)
        {
            hpactual -= 80;
            hpMax -= 80;
            HpSuma = false;
        }

        maxHealth = hpMax;
        currenthealth = hpactual;
        hpMax = maxHealth;
        barraVida.SetHealth(currenthealth);

        if (hpactual <= 0 || hpMax<=0)
        {
            if (Revivio == true)
            {
                hpactual = hpMax;
                Rev.SetActive(false);
            }
            else
            {
                //this.gameObject.SetActive(false);
                GameObject go = GameObject.Find("Canvas");
                Cargar_Scena ec = go.GetComponent<Cargar_Scena>();
                ec.GameOver();
                GameObject hey = GameObject.Find("SistemadeTurnos");
                Turnos ke = hey.GetComponent<Turnos>();
                ke.Reset();
            }
            CardDisplay.Revive = false;
        }

    }

    public void brebaje()
    {
        if (CardDisplay.aunment > 0)
        {
            brebaj.SetActive(true);
        }
        else
        {
            brebaj.SetActive(false);
        }
    }      
    
    
    public void CristoRey()
    {
        if (CardDisplay.Revive == true)
        {
            Revivio = true;
            Rev.SetActive(true);
        }
        else
        {
            Revivio = false;
            Rev.SetActive(false);
        }
    }    
    
    public void esquive()
    {
        if (CardDisplay.esquivePl == 1)
        {
            esquiv.SetActive(true);
        }
        else
        {
            esquiv.SetActive(false);
        }
    }
}

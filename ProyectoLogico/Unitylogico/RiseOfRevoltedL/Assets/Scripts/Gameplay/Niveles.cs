﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Niveles : MonoBehaviour
{
    public GameObject mapa;
    public GameObject nivel1;
    public GameObject nivel2;
    public GameObject nivel3;
    public GameObject nivel4;
    public GameObject nivel5;
    public GameObject nivel6;
    public GameObject nivel7;
    public static int nvl;
    public int nivel = 1;

    // Start is called before the first frame update
    void Start()
    {
        nvl = nivel;
        mapa.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        nivel = nvl;
        switch (nvl)
        {
            case 1: nivel1.SetActive(true); break;
            case 2: nivel2.SetActive(true); break;
            case 3: nivel3.SetActive(true); break;
            case 4: nivel4.SetActive(true); break;
            case 5: nivel5.SetActive(true); break;
            case 6: nivel6.SetActive(true); break;
            case 7: nivel7.SetActive(true); break;
        }
    }
}

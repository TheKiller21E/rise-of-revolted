﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Turnos : MonoBehaviour
{
    public static bool isYourTurn;
    public static bool isEnemyTurn;
    public bool YourTurn;
    public bool EnemyTurn;
    public static int EnemyTurnPerder;
    public static int PlayerTurnPerder;
    public int enemyTurnPerder;
    public int playerTurnPerder;
    public int yourturn;
    public int enemyturn;
    public GameObject turnnext;
    public GameObject turntext;
    [HideInInspector]
    public int maxMana;
    public int currentMana;
    public Text Mana;
    public static int ManaActual;
    public static bool Turnoempieza;
    public GameObject Boton_turno;


    // Start is called before the first frame update
    void Start()
    {
        isYourTurn = true;
        YourTurn= isYourTurn;
        EnemyTurn= isEnemyTurn;
        yourturn = 1;
        enemyturn = 0;

        maxMana = 10;
        currentMana = 10;
        ManaActual = currentMana;

        Turnoempieza = false;
        Boton_turno = GameObject.Find("Barajear_Terminarturno");
    }

    // Update is called once per frame
    void Update()
    {
        playerTurnPerder = PlayerTurnPerder;
        enemyTurnPerder = EnemyTurnPerder;


        currentMana = ManaActual;
        if (CardDisplay.loseTurn==true)
        {
            EnemyTurnPerder = 1;
            CardDisplay.loseTurn = false;
        }
        if (CardDisplay.loseTurnPl == true)
        {
            PlayerTurnPerder = 1;
            CardDisplay.loseTurnPl = false;
        }

        if (YourTurn==true)
        {
            turnnext.SetActive(true);
            turntext.GetComponent<Text>().text= "Tu Turno";
            StartCoroutine(Hide());
            YourTurn = false;
            Boton_turno.GetComponent<Button>().enabled = true;
            Player.defensa = 0;

        }

        if (EnemyTurn==true)
        {
            turnnext.SetActive(true);
            turntext.GetComponent<Text>().text = "Turno Enemigo";
            StartCoroutine(Hide());
            EnemyTurn = false;
            Boton_turno.GetComponent<Button>().enabled = false;
            Enemy.defensa = 0;
        }
        Mana.text = currentMana + " / " + maxMana;

    }

    public void TerminarTurno()
    {
        if (EnemyTurnPerder == 1)
        {
            CardDisplay.loseTurn = false;
            ManaActual = 10;
            EnemyTurnPerder = 0;
            TerminarTurnoEnemigo();
        }
        else
        {
            enemyturn += 1;
            isEnemyTurn = true;
            isYourTurn = false;
            EnemyTurn = isEnemyTurn;
            ManaActual = 10;
        }
    }

    public void TerminarTurnoEnemigo()
    {
        if (PlayerTurnPerder == 1)
        {
            CardDisplay.loseTurnPl = false;
            CardDisplay.aunment--;
            Enemy.tocaT += 1;
            PlayerTurnPerder = 0;
            TerminarTurno();
        }
        else
        {
            isYourTurn = true;
            isEnemyTurn = false;
            YourTurn = isYourTurn;
            yourturn += 1;
            Turnoempieza = true;
            CardDisplay.esquivePl = 0;
            if (CardDisplay.aunment > 0)
            {
                CardDisplay.aunment--;
            }
        }

    }

    IEnumerator Hide()
    {
        yield return new WaitForSeconds(2);
        turnnext.SetActive(false);
    }

    public void Reset()
    {
        Turnoempieza = true;
        YourTurn = true;
        isYourTurn = YourTurn;
        EnemyTurn = false;
        isEnemyTurn = EnemyTurn;
        yourturn = 0;
        enemyturn = 0;
        ManaActual = 10;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class VidaEnemigo : MonoBehaviour
{

    public Slider slider;
    public Text hptext;

    public void SetMaxHealth(int vida)
    {
        slider.value = vida;
        slider.maxValue = vida;
    }


    public void SetHealth(int vida)
    {
        slider.value = vida;
    }

    void Update()
    {
        hptext.text = Enemy.hpactual + " / " + Enemy.hpMax;
    }
}

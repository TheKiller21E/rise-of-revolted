﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Descripciones : MonoBehaviour
{
    public GameObject Cuadro;
    public Text descrip;

    public void AparecerEsqui()
    {
        Cuadro.SetActive(true);
        descrip.text = "Se esquivaran todos los siguientes ataques"; 
    }

    public void AparecerConf()
    {
        Cuadro.SetActive(true);
        descrip.text = "El enemigo se hara daño a si mismo por tonto";
    }

    public void AparecerTdamg()
    {
        Cuadro.SetActive(true);
        descrip.text = "El enemigo destruye tu escudo y te hace daño";
    }

    public void AparecerBreba()
    {
        Cuadro.SetActive(true);
        descrip.text = "Te has vuelto un berserker, te quedan " + CardDisplay.aunment + " turnos como uno";
    }

    public void off()
    {
        Cuadro.SetActive(false);
    }
}
